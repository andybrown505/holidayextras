﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HolidayExtras.API.Models
{
    public class User
    {
        [Key]
        public int id { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Please supply a vaild email address")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [Required]
        [StringLength(50)]
        public string givenName { get; set; }

        [Required]
        [StringLength(50)]
        public string familyName { get; set; }

        [StringLength(50)]
        public string middleName { get; set; }

        [DataType(DataType.Date)]
        public DateTime dob { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime created { get; set; }
    }
}
