﻿using System;
using System.Collections.Generic;
using System.Linq;

using HolidayExtras.API.Models;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HolidayExtras.API.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserContext _context;
        private readonly ILogger _logger;

        public UserController(ILogger<UserController> logger, UserContext context)
        {
            //init the app
            _context = context;
            _logger = logger;
            PopulateContext();
        }

        [HttpGet("{id}")]
        public ActionResult<User> GetById(int id)
        {
            //search for the user by id
            var user = _context.Users.Find(id);

            //if no user with the id is found then return a 404
            if((user is null))
            {
                _logger.LogWarning("No user with an id of {ID} was found.", id);
                return NotFound();
            }

            //if the user is found return the json user object
            return user;
        }

        [HttpGet]
        [Route("~/api/user/getall")]
        public ActionResult<List<User>> GetAll()
        {
            return _context.Users.ToList();
        }

        [HttpPost]
        public ActionResult<User> Create([FromBody]User user)
        {
            if(!ModelState.IsValid)
            {
                _logger.LogWarning("There was an error processing your request");
                return BadRequest(ModelState);
            }

            //get the next available id for the new user
            var nextId = _context.Users.Count() + 1;
            user.id = nextId;

            //set the time that the user was created
            user.created = DateTime.Now;

            try
            {
                _context.Add(user);
                _context.SaveChanges();
            }
            catch(Exception ex)
            {
                _logger.LogWarning("There was an error processing your request");
                return BadRequest(ex.Message);
            }

            _logger.LogInformation("Created usser with id {ID}", user.id);
            return Ok(string.Format("User with id of {0} was created.", user.id));
        }

        [HttpPut]
        public ActionResult<User> Update([FromBody]User user)
        {
            if(!ModelState.IsValid)
            {
                _logger.LogWarning("There was an error processing your request");
                return BadRequest(ModelState);
            }

            try
            {
                _context.Update(user);
                _context.SaveChanges();
            }
            catch(Exception ex)
            {
                _logger.LogWarning("There was an error processing your request");
                return BadRequest(ex.Message);
            }

            _logger.LogInformation("Updated user with id {ID}", user.id);
            return Ok(user.id);
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var user = _context.Users.Find(id);

            if(user is null)
            {
                _logger.LogWarning("No user with id {ID} was found", id);
                return NotFound();
            }

            try
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
            }
            catch(Exception ex)
            {
                _logger.LogWarning("There was an error processing your request");
                return BadRequest(ex.Message);
            }

            return NoContent();
        }

        /// <summary>
        /// populates the context with test data if it is empty
        /// </summary>
        private void PopulateContext()
        {
            if(_context.Users.Count().Equals(0))
            {
                _context.Users.Add(new Models.User { id = 1, email = "test1@test.com", givenName = "John", familyName = "Doe", created = DateTime.Now });
                _context.Users.Add(new Models.User { id = 2, email = "test2@test.com", givenName = "Jane", familyName = "Doe", created = DateTime.Now });
                _context.Users.Add(new Models.User { id = 3, email = "test3@test.com", givenName = "James", familyName = "Doe", created = DateTime.Now });
                _context.Users.Add(new Models.User { id = 4, email = "test4@test.com", givenName = "Janet", familyName = "Doe", created = DateTime.Now });
                _context.Users.Add(new Models.User { id = 5, email = "test5@test.com", givenName = "Jake", familyName = "Doe", created = DateTime.Now });
                _context.SaveChanges();
            }
        }
    }
}
